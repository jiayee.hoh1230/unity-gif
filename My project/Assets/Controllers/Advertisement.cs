using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using ThreeDISevenZeroR.UnityGifDecoder;

public class Advertisement : MonoBehaviour
{
    [SerializeField] private Material advertisementMaterial = null;
    [SerializeField] private GameObject advertismentPlaneObject = null;
    // [SerializeField] private string advetisementImageUrl = null;

    private float update;
    private List<Texture> frames;
    private List<float> frameDelays;
    private int frameRate;
    private float updateInterval = 0.01f;


    private float updateCount = 0;
    private float fixedUpdateCount = 0;
    private float updateUpdateCountPerSecond;
    private float updateFixedUpdateCountPerSecond;

    void Awake()
    {
        StartCoroutine(Loop());
    }

    async void Start()
    {
        // Texture2D texture = await RemoteTextureLoader.LoadTexture(advetisementImageUrl);
        // advertisementMaterial.mainTexture = texture;
        // advertismentPlaneObject.GetComponent<MeshRenderer>().material = advertisementMaterial;

        // Debug.Log("Start1");
        // yield return new WaitForSeconds(0.1f);
        // Debug.Log("Start2");
        // InvokeRepeating("UpdateInterval",updateInterval,updateInterval);

        // test Unity-GifDecoder
        frames = new List<Texture>();
        frameDelays = new List<float>();
        // var fileLoca = Resources.Load("/tenor.0.gif");
        // var gifStream = new GifStream("../GifFile/tenor.0.gif");

        using (var gifStream = new GifStream("./Assets/GifFile/why_sponge.gif"))
        {
            while (gifStream.HasMoreData)
            {
                switch (gifStream.CurrentToken)
                {
                    case GifStream.Token.Image:
                        var image = gifStream.ReadImage();
                        var frame = new Texture2D(
                            gifStream.Header.width, 
                            gifStream.Header.height, 
                            TextureFormat.ARGB32, false); 

                        frame.SetPixels32(image.colors);
                        frame.Apply();

                        frames.Add(frame);
                        frameDelays.Add(image.SafeDelaySeconds); // More about SafeDelay below
                        break;
                    
                    case GifStream.Token.Comment:
                        var commentText = gifStream.ReadComment();
                        Debug.Log(commentText);
                        break;

                    default:
                        gifStream.SkipToken(); // Other tokens
                        break;
                }
            }
        }
    }

    void UpdateInterval()
    {
        //use this as the secondary update.
         if(frames != null){
            var framesPerSecond = 30;
            var index = (int)Time.time * framesPerSecond; 
            // float dd =  index % frames.Count;
            index = index % frames.Count;
            advertisementMaterial.mainTexture = frames[index];
            advertismentPlaneObject.GetComponent<MeshRenderer>().material = advertisementMaterial;
        }
    }

    void Update()
    {
        // update += Time.deltaTime;
        // if (update > 1.0f)
        // {
        //     update = 0.0f;
        //     Debug.Log("Update");
        // }
        updateCount += 1;
        // Debug.Log("updateCount: " + updateCount);
    }

    void FixedUpdate()
    {
        fixedUpdateCount += 1;
        // Debug.Log("fixedUpdateCount: " + fixedUpdateCount);
        if(frames != null){
            // Debug.Log("fixedUpdateCount: " + fixedUpdateCount);
            int framesPerSecond = 60;
            var index = (int)Time.time * framesPerSecond; 
            // Debug.Log(Time.time * 0.1);
            index = index % frames.Count;
            Debug.Log(index % frames.Count);
            advertisementMaterial.mainTexture = frames[index];
            advertismentPlaneObject.GetComponent<MeshRenderer>().material = advertisementMaterial;
        }
    }

    IEnumerator Loop()
    {
        while (true)
        {
            yield return new WaitForSeconds(0.01f);
            updateUpdateCountPerSecond = updateCount;
            updateFixedUpdateCountPerSecond = fixedUpdateCount;

            updateCount = 0;
            fixedUpdateCount = 0;
        }
    }
}
